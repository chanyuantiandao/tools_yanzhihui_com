<?php
if(!defined('IN_DISO')){
	Header('HTTP/1.1 404 Not Found');
	Header('Status: 404 Not Found');
	exit;
}

$xmlContent = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<tools>
	<category name="帮助文档">
		<tool name="CSS文档" pubdate="2021-08-04" icon="css.png" description="比较完整的CSS文档" link="http://css.yanzhihui.com/" />
		<tool name="JQuery1.x文档" pubdate="2021-08-04" icon="jquery.jpg" description="JQuery1.11.3文档" link="http://jquery.yanzhihui.com/" />
		<tool name="JQuery3.x文档" pubdate="2021-08-04" icon="jquery.jpg" description="JQuery3.3.1文档" link="http://jquery3.yanzhihui.com/" />
		<tool name="C/C++文档" pubdate="2021-08-09" icon="cplus_standard_library.jpg" description="C/C++帮助文档" link="https://en.cppreference.com/w/" />
		<tool name="程序员的手册库" pubdate="2021-08-14" icon="" description="各种编程手册的集合库" link="http://shouce.jb51.net/" />
	</category>
	<category name="工具软件文档">
		<tool name="Git" pubdate="2021-08-10" icon="git.png" description="Git命令详解" link="http://doc.yanzhihui.com/doku.php/wiki:git" />
		<tool name="LaTeX" pubdate="2021-08-10" icon="latex.png" description="latex入门知识" link="http://doc.yanzhihui.com/doku.php/wiki:latex" />
	</category>
	<category name="网站收集">
		<tool name="RGB HSV转换" pubdate="2021-08-04" icon="rgbhsv.png" description="RGB HSV 互转工具" link="https://www.codercto.com/tool/rgb_hsv.html" />
		<tool name="正则测试工具" pubdate="2021-08-04" icon="" description="正则表达式测试工具，还可以生成代码哦" link="https://tool.lu/regex/" />
		<tool name="ICON图标搜索" pubdate="2021-08-04" icon="iconfider.png" description="ICON资源搜索网站，免费的很少" link="https://www.iconfinder.com/" />
		<tool name="LOLCOLORS" pubdate="2021-08-14" icon="lolcolors.svg" description="配色网站" link="https://www.webdesignrankings.com/resources/lolcolors/" />
	</category>
	<category name="生活相关网站">
		<tool name="天地图" pubdate="2021-08-04" icon="" description="国家地理信息公共服务平台" link="https://map.tianditu.gov.cn/" />
	</category>
	<category name="仿真计算">
		<tool name="多物理场仿真百科" pubdate="2021-08-04" icon="physical.png" description="多物理场仿真百科" link="http://cn.comsol.com/multiphysics" />
	</category>
	<category name="个人网站">
		<tool name="智慧的Blog" pubdate="2021-08-07" icon="" description="智慧的博客" link="http://www.yanzhihui.com/" />
		<tool name="智慧的知识库" pubdate="2021-08-07" icon="" description="智慧的学习知识库" link="http://doc.yanzhihui.com/" />
		<tool name="本章源码下载" pubdate="2021-08-07" icon="" description="本站源码的gitee仓库" link="https://gitee.com/chanyuantiandao/tools_yanzhihui_com" />
	</category>
</tools>
XML;
