<?php
if(!defined('IN_DISO')){
	Header('HTTP/1.1 404 Not Found');
	Header('Status: 404 Not Found');
	exit;
}

function compress_html($string) {
    $string = str_replace("\r\n", '', $string); //清除换行符
    $string = str_replace("\n", '', $string); //清除换行符
    $string = str_replace("\t", '', $string); //清除制表符
    $pattern = array(
        "/> *([^ ]*) *</", //去掉注释标记
        "/[\s]+/",
        "/<!--[^!]*-->/",
        "/\" /",
        "/ \"/",
        "'/\*[^*]*\*/'"
    );
    $replace = array(
        ">\\1<",
        " ",
        "",
        "\"",
        "\"",
        ""
    );
    return preg_replace($pattern, $replace, $string);
}

function chanSubstr($string, $length = 35, $start = 0){
	if(strlen($string) > $length){
		$string = mb_substr($string, $start, $length).'...';
	}
	return $string;
	
}
function getNodesFromXmlString($xmlString){
	$doc = new DOMDocument();
	$doc->loadXML($xmlString);
	return $doc->getElementsByTagName('category');
}

function getNodesFromXmlFile($xmlFile){
	$doc = new DOMDocument();
	$doc->load($xmlFile);
	return $doc->getElementsByTagName('category');
}

function getAttribute($domElement, $attrname){
	if($domElement->hasAttribute($attrname)){
		return $domElement->getAttribute($attrname);
	}else if(!empty($domElement->getElementsByTagName($attrname))){
		return $domElement->getElementsByTagName($attrname)[0]->nodeValue;
	}else{
		return '';
	}
}