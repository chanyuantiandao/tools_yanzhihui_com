<?php 
define('IN_DISO', TRUE);

if(file_exists('index.cache.php') && filemtime('index.cache.php') > filemtime('tools.xml.php') && filemtime('index.cache.php') > filemtime('index.php')){
	include_once('index.cache.php');
	exit;
}
require_once('./index.function.php');
require_once('./tools.xml.php');
ob_start();
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="shortcut icon" href="./favicon.ico" />
<title>智慧的工具箱</title>
<meta name="keywords" content="工具箱,手册,网站收藏" />
<meta name="description" content="智慧的工具箱，保持持续更新收藏">
<link href="style.css" rel="stylesheet" />
</head>
<body>
	<header>
		<span class="logo"><a href="http://tools.yanzhihui.com/">智慧的工具箱</a></span>
		<span class="contact"><a href="mailto:chanyuantiandao@126.com">联系站长</a></span>
	</header>
	<div class="content">
		<?php 
			$categroies = getNodesFromXmlString($xmlContent);
			foreach ($categroies as $categroy) {
				$tools = $categroy->getElementsByTagName('tool');
		?>
		<section>
			<div class="title"><span class="name"><?php echo $categroy->getAttribute('name'); ?></span><span class="more"><a href="javascript:void(0);">更多</a></span></div>
			<ul class="tools">
				<?php
				foreach ($tools as $tool) {
				?>
				<li>
					<div class="t">
						<?php $icon = getAttribute($tool, 'icon');  ?>
						<img class="icon" src="<?php echo $icon == '' ? 'app.png' : $icon; ?>" />
						<div>
							<h3 class="name"><a href="<?php echo getAttribute($tool, 'link'); ?>" target="_blank"><?php echo getAttribute($tool, 'name'); ?></a></h3>
							<p><span class="collect"><?php echo getAttribute($tool, 'pubdate'); ?></span><span class="categroy">[<a href="http://www.yanzhihui.com/" target="_blank">禅元天道</a>]</span></p>
						</div>
					</div>
					<p class="desc"><?php echo chanSubstr(getAttribute($tool, 'description'), 48); ?></p>
					<p><span class="link"><a href="<?php echo getAttribute($tool, 'link'); ?>" target="_blank"><?php echo chanSubstr(getAttribute($tool, 'link'),30); ?></a></span><span class="go"><a href="<?php echo getAttribute($tool, 'link'); ?>" target="_blank">前往</a></span></p>
				</li>
				<?php
				}
				?>
			</ul>
		</section>	
		<?php
			}
		?>	
	</div>
	<footer>
		<p>Copyright © 智慧的工具箱 All Rights Reserved. ICP备案号：辽ICP备13002897号</p>
	</footer>
</body>
</html>
<?php
	$content = ob_get_contents();
	file_put_contents('index.cache.php', '<?php if(!defined("IN_DISO")){Header("HTTP/1.1 404 Not Found");Header("Status: 404 Not Found");	exit;} ?>' . compress_html($content));
?>